package ru.t1consulting.vmironova.tm.api.repository;

import ru.t1consulting.vmironova.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(M model);

    List<M> findAll();

    List<M> findAll(Comparator comparator);

    boolean existsById(String id);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    void clear();

    M remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);

    int getSize();

    void removeAll(Collection<M> collection);

}
