package ru.t1consulting.vmironova.tm.enumerated;

import java.util.Arrays;
import java.util.Optional;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    private final String displayName;

    Status(final String displayName) {
        this.displayName = displayName;
    }

    public static String toName(final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    public static Status toStatus(final String value) {
        if (value == null || value.isEmpty()) return null;
        final Optional<Status> statusOptional = Arrays.stream(values()).filter(m -> value.equals(m.name())).findFirst();
        return statusOptional.orElse(null);
    }

    public String getDisplayName() {
        return displayName;
    }

}
