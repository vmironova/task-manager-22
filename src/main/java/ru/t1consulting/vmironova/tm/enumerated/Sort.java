package ru.t1consulting.vmironova.tm.enumerated;

import ru.t1consulting.vmironova.tm.comparator.CreatedComparator;
import ru.t1consulting.vmironova.tm.comparator.NameComparator;
import ru.t1consulting.vmironova.tm.comparator.StatusComparator;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;

public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE);

    private final String displayName;

    private final Comparator comparator;

    Sort(final String displayName, final Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public static Sort toSort(final String value) {
        if (value == null || value.isEmpty()) return null;
        final Optional<Sort> sortOptional = Arrays.stream(values()).filter(m -> value.equals(m.name())).findFirst();
        return sortOptional.orElse(null);
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

}
