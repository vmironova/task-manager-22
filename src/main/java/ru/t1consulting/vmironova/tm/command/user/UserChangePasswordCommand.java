package ru.t1consulting.vmironova.tm.command.user;

import ru.t1consulting.vmironova.tm.enumerated.Role;
import ru.t1consulting.vmironova.tm.model.User;
import ru.t1consulting.vmironova.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Change user password.";

    public static final String NAME = "user-change-password";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String newPassword = TerminalUtil.nextLine();
        final User user = getAuthService().getUser();
        getUserService().setPassword(user.getId(), newPassword);
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
