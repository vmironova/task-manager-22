package ru.t1consulting.vmironova.tm.command.user;

import ru.t1consulting.vmironova.tm.enumerated.Role;
import ru.t1consulting.vmironova.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "View profile of current user.";

    public static final String NAME = "user-view-profile";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER PROFILE]");
        final User user = getAuthService().getUser();
        showUser(user);
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
