package ru.t1consulting.vmironova.tm.command.system;

import ru.t1consulting.vmironova.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgumentListCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-arg";

    public static final String DESCRIPTION = "Show application arguments.";

    public static final String NAME = "arguments";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        commands.forEach(m -> {
            final String argument = m.getArgument();
            if (argument != null && !argument.isEmpty()) System.out.println(argument);
        });
    }

}
