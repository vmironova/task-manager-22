package ru.t1consulting.vmironova.tm.command.system;

import ru.t1consulting.vmironova.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandListCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-cmd";

    public static final String DESCRIPTION = "Show application commands.";

    public static final String NAME = "commands";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        commands.forEach(m -> {
            final String name = m.getName();
            if (name != null && !name.isEmpty()) System.out.println(name);
        });
    }

}
