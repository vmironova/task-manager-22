package ru.t1consulting.vmironova.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-a";

    public static final String DESCRIPTION = "Show developer info.";

    public static final String NAME = "about";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Veronika Mironova");
        System.out.println("E-mail: vmironova@t1-consulting.ru");
        System.out.println("E-mail: mironovavg2@vtb.ru");
    }

}
