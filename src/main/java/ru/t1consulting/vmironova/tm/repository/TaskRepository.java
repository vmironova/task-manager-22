package ru.t1consulting.vmironova.tm.repository;

import ru.t1consulting.vmironova.tm.api.repository.ITaskRepository;
import ru.t1consulting.vmironova.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        return models.stream()
                .filter(m -> projectId.equals(m.getProjectId()))
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        final Task task = new Task(name, description);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public Task create(final String userId, final String name) {
        final Task task = new Task(name);
        task.setUserId(userId);
        return add(task);
    }

}
