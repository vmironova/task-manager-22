package ru.t1consulting.vmironova.tm.component;

import ru.t1consulting.vmironova.tm.api.repository.ICommandRepository;
import ru.t1consulting.vmironova.tm.api.repository.IProjectRepository;
import ru.t1consulting.vmironova.tm.api.repository.ITaskRepository;
import ru.t1consulting.vmironova.tm.api.repository.IUserRepository;
import ru.t1consulting.vmironova.tm.api.service.*;
import ru.t1consulting.vmironova.tm.command.AbstractCommand;
import ru.t1consulting.vmironova.tm.command.project.*;
import ru.t1consulting.vmironova.tm.command.system.*;
import ru.t1consulting.vmironova.tm.command.task.*;
import ru.t1consulting.vmironova.tm.command.user.*;
import ru.t1consulting.vmironova.tm.enumerated.Role;
import ru.t1consulting.vmironova.tm.enumerated.Status;
import ru.t1consulting.vmironova.tm.exception.system.ArgumentNotSupportedException;
import ru.t1consulting.vmironova.tm.exception.system.CommandNotSupportedException;
import ru.t1consulting.vmironova.tm.model.Project;
import ru.t1consulting.vmironova.tm.model.User;
import ru.t1consulting.vmironova.tm.repository.CommandRepository;
import ru.t1consulting.vmironova.tm.repository.ProjectRepository;
import ru.t1consulting.vmironova.tm.repository.TaskRepository;
import ru.t1consulting.vmironova.tm.repository.UserRepository;
import ru.t1consulting.vmironova.tm.service.*;
import ru.t1consulting.vmironova.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ApplicationHelpCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new ApplicationAboutCommand());
        registry(new ApplicationVersionCommand());
        registry(new ApplicationInfoCommand());
        registry(new ApplicationExitCommand());

        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserViewProfileCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());
        registry(new UserRemoveCommand());

        registry(new ProjectListCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());

        registry(new TaskListCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    private void initDemoData() {
        final User admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);
        final User test = userService.create("TEST", "TEST");

        projectService.add(test.getId(), new Project("First Project", Status.IN_PROGRESS));
        projectService.add(test.getId(), new Project("Second Project", Status.NOT_STARTED));

        taskService.create(test.getId(), "First Task");
        taskService.create(test.getId(), "Second Task");
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void processArgument(final String argument) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(final String[] args) {
        if (processArguments(args)) System.exit(0);
        initDemoData();
        initLogger();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                processCommand(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

}
